import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class MonitoredData {
 
	Date start_time;
	Date end_time;
	String activity;
	SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	
	public MonitoredData(String start, String end, String act) throws ParseException {
		start_time = sdf.parse(start);
		end_time = sdf.parse(end);
		activity = act;
	}
	
	
	public Date getDate1() {
		return start_time;
	}
	
	public Date getDate2() {
		return end_time;
	}
	
	public String getActivity() {
		return activity;
	}
	
 public int getDayof1() {
	 return start_time.getDate();
 }
	
}
