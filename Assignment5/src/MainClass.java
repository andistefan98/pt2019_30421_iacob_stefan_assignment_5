import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import static java.lang.Math.toIntExact;

public class MainClass {
	
	
	@SuppressWarnings("deprecation")
	public static void main(String[] args) throws IOException, ParseException
	{
	    MainClass cls1  = new MainClass();
	    String space= "         ";
	    String newLine= "\n ";
	    //MonitoredData dt = new MonitoredData(null,null,"");
	    List<MonitoredData> dataM = new ArrayList<MonitoredData>();
	    File fl;
	    Map<String,Integer> activitiesCount = new HashMap<String,Integer>();
	    File outputFile = new File("output1.txt");
	    File outputFile3 = new File("output3.txt");
	    FileOutputStream fos = new FileOutputStream(outputFile);
	    FileOutputStream fos3 = new FileOutputStream(outputFile3);
	    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");


	    List<MonitoredData> activ = new ArrayList<MonitoredData>();
	    fl = new File("D:\\ecg/Activities.txt");

	    int cnt = 0;

	    activ = Files.lines(fl.toPath())
	    .map(el -> el.split("\\s+"))
	    .map(el ->  {
	        try
	        {
	            return new MonitoredData((el[0]=el[0].concat(" ").concat(el[1])),(el[2]=el[2].concat(" ").concat(el[3])),el[4]);
	        }
	        catch (ParseException e)
	        {
	            // TODO Auto-generated catch block
	            e.printStackTrace();
	        }
	        return null;
	    })

	    .collect(Collectors.toList())
	    ;


	    System.out.println("\nExercise 1: \n");

	    for(MonitoredData str: activ)
	    {
	    	byte[][] bytesArr = new byte[4][];
	    	bytesArr[1] = str.getDate1().toString().getBytes();
	    	bytesArr[2] = str.getDate2().toString().getBytes();
	    	bytesArr[0] = str.getActivity().getBytes();
	    	fos.write(bytesArr[0]);
	    	fos.write(space.getBytes());
	    	fos.write(bytesArr[1]);
	    	fos.write(space.getBytes());
	    	fos.write(bytesArr[2]);
	    	fos.write(newLine.getBytes());
            
	        System.out.println( str.getDate1() + "  " + str.getDate2()  + "  " + str.getActivity() );

	    }


	    System.out.println("\nExercise 2 (days of monitored data) : \n");

	    int noDays = cls1.findNoDays2(activ);

	    System.out.println("Number of days  :  " + noDays + "\n\n\n");

	    System.out.println("\nExercise 3 (how many times each activity appeared) : \n");

	    activitiesCount = cls1.findCount(activ);
	    
	    for (String activity: activitiesCount.keySet())
	    {
	        String key = activity;
	        Integer value = activitiesCount.get(activity);
	        fos3.write(key.getBytes());
	        fos3.write(space.getBytes());
	        fos3.write(value.toString().getBytes());
	        fos3.write(newLine.getBytes());
	        
	    }


	    System.out.println("\nExercise 4(each activity for each day): \n");

	    cls1.daysAndActivities(activ);

	    System.out.println("\nExercise 5: \n");

	    cls1.activitiesWithDuration(activ);

	    System.out.println("\nExercise 6(entire duration in minutes): \n");

	    cls1.computeTotalDurations(activ);

	    System.out.println("\nExercise 7 ( 90% under 5 minutes): \n");

	    cls1.over90(activ);

	    System.out.println("\n\n");




	}
	
	int findNoDays2(List<MonitoredData> dataM) {
		  long sizee = dataM.stream().map(el->el.getDate1().getDate())
		    	    .distinct()
		    	    .count();
		  
		  int sizeF= toIntExact(sizee);
		  
		return sizeF;
		    
	}




	@SuppressWarnings("deprecation")
	int findNoDays(List<MonitoredData> dataM)
	{

	    List<Date> arrDates1 = new ArrayList<Date>(300);

	    arrDates1 = dataM.stream()
	                .map(el -> el.getDate1())
	                .sorted()
	                .limit(1)
	                .collect(Collectors.toList());

	    List<Date> arrDates2 = new ArrayList<Date>();

	    arrDates2 = dataM.stream()
	                .map(el -> el.getDate2())
	                .sorted(Comparator.reverseOrder())
	                .limit(1)
	                .collect(Collectors.toList());

	    if(arrDates1.get(0).getDate() > arrDates2.get(0).getDate())
	    {
	        if(arrDates1.get(0).getHours()  > arrDates2.get(0).getHours())
	        {
	            return 30 - arrDates1.get(0).getDate() + arrDates2.get(0).getDate();
	        }
	        else
	        {
	            return 30 - arrDates1.get(0).getDate() + arrDates2.get(0).getDate()+ 1;
	        }
	    }
	    else
	    {
	        return arrDates2.get(0).getDate() - arrDates1.get(0).getDate();
	    }
	    
	 

	}





	Map<String,Integer> findCount(List<MonitoredData> listM)
	{


	    Map<String, Long> act = new HashMap<String,Long>();
	    act = listM.stream()
	          .collect(Collectors.groupingBy((MonitoredData md) -> md.getActivity(),Collectors.counting()));

	    Map<String, Integer> resF = new HashMap<String,Integer>();

	    for(String activity : act.keySet())
	    {
	        resF.put(activity, toIntExact(act.get(activity)));
	    }

	    for (String activity: resF.keySet())
	    {
	        String key = activity;
	        Integer value = resF.get(activity);
	        System.out.println( "Activity : " + key + "  for  " + value + "  times .");
	        
	    }


	    return resF;

	}


	void over90(List<MonitoredData> dataM)
	{
	    Map<String,Integer> mapF = findCount(dataM);
	    Map<String,Integer> under5 = new HashMap<String,Integer>();

	    List<String> activities = dataM.stream().map(el -> el.getActivity()).sorted().collect(Collectors.toList());
	    Map<String, Long> activitiesUnder5 = new HashMap<String,Long>();

	    Map<String,Float> overr= new HashMap<String,Float>();

	    //list with activities all under 5 minutes
	    activitiesUnder5 = dataM.stream()
	                       .filter(el -> (el.getDate2().getTime() - el.getDate1().getTime())/1000 < 300 )
	                       .collect(Collectors.groupingBy((MonitoredData md)-> md.getActivity(),Collectors.counting()));

	    for(String activity : activitiesUnder5.keySet())
	    {
	        under5.put(activity, toIntExact(activitiesUnder5.get(activity)));
	    }


	    HashSet<String> res= new HashSet<String>();
	    for(int i=0; i < activities.size(); i++)
	    {
	        if(under5.get(activities.get(i)) != null)
	        {
	            if(under5.get(activities.get(i)) > 0.9 * mapF.get(activities.get(i)))
	            {
	                overr.put(activities.get(i), (float) under5.get(activities.get(i))*100/(float)mapF.get(activities.get(i)));
	            }
	        }
	    }

	    System.out.println("\n");

	    for (String activity: overr.keySet())
	    {
	        String key = activity;
	        Float value = overr.get(activity);
	        System.out.println(key + "   " + value);
	    }
	}




	void activitiesWithDuration(List<MonitoredData> dataM)
	{
	    DecimalFormat df = new DecimalFormat();
	    df.setMaximumFractionDigits(2);

	    dataM.stream().forEach(MonitoredData->
	    {
	        System.out.println(MonitoredData.getActivity() + "   " +df.format((float) (((MonitoredData.getDate2().getTime() - MonitoredData.getDate1().getTime())/(float)60000)) ));
	    }

	                 );


	}



	void daysAndActivities(List<MonitoredData> dataM)
	{

	    List<String> act = new ArrayList<String>();

	    Map<Integer, Map<String, Long>> data2 =  new HashMap<Integer, Map<String, Long>>();
	    data2 = dataM.stream()
	            .collect(Collectors.groupingBy(MonitoredData::getDayof1,
	                                           Collectors.groupingBy(MonitoredData::getActivity,Collectors.counting())));

	    for (Integer dayNo: data2.keySet())
	    {

	        Map<String, Long> value = data2.get(dayNo);

	        System.out.println("\nDay no.  "  + dayNo +"\n");
	        for(String dm: value.keySet())
	        {
	            System.out.println(dm + "     " + value.get(dm));
	        }
	    }

	}


	Map<String,Float> computeTotalDurations(List<MonitoredData> dataM)
	{

	    Map<String,Float> mapFinal = new HashMap<String,Float>();

	    List<String> getActivities = new ArrayList<String>();
	    getActivities = dataM.stream().map(el -> el.getActivity())
	                    .distinct()
	                    .collect(Collectors.toList());
	    
	    

	    for(String str: getActivities)
	    {
	        mapFinal.put(str, (float)0);
	    }

	    dataM.forEach(MonitoredData->
	    {
	        mapFinal.put(MonitoredData.getActivity(),(float) (mapFinal.get(MonitoredData.getActivity()) + ((MonitoredData.getDate2().getTime() - MonitoredData.getDate1().getTime())/(float)1000)/60   ));
	    });

	    //displaying the resulted map
	    DecimalFormat df = new DecimalFormat();
	    df.setMaximumFractionDigits(2);

	    for (String activity: mapFinal.keySet())
	    {
	        String key = activity;
	        Float value = mapFinal.get(activity);
	        System.out.println(key + "   " + df.format(value));
	    }

	    return mapFinal;

	}
		
	
}
